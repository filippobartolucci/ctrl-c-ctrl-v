import os
import sys, os
import pathlib
import warnings
import itertools
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import tensorflow_datasets as tfds
from sklearn.utils import class_weight
from sklearn.model_selection import KFold
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from keras.applications.mobilenet_v2 import preprocess_input


warnings.filterwarnings("ignore")
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
tf.random.set_seed(42)

# Path setting
mel_path = 'mel_img/'
scg_path = 'scg_img/'
aug_path = 'aug_img/'
data_dir = pathlib.Path(mel_path)

# Model Settings
img_height = 224
img_width = 224

SEED = 13
TEST_SPLIT = 0.25
VALIDATION_SPLIT = 0.20
MAX_EPOCHS = 13
EARLY_STOPPING_PATIENCE = 7
BASE_LEARNING_RATE = 0.0001
BETA = 0.9999

# per-fold score containers
acc_per_fold = []
loss_per_fold = []
pred_per_fold = []
num_fold = 7
n_fold = 1
kfold = KFold(n_splits=num_fold, shuffle=True, random_state=SEED)

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()


def ModelBuilder():
    img_shape = (img_width, img_height) + (3,)
    base_model = tf.keras.applications.MobileNetV2(input_shape=img_shape,
                                                   include_top=False,
                                                   weights='imagenet')

    image_batch, label_batch = next(iter(train_ds))

    feature_batch = base_model(image_batch)

    base_model.trainable = True
    global_average_layer = tf.keras.layers.GlobalAveragePooling2D()
    feature_batch_average = global_average_layer(feature_batch)
    prediction_layer = tf.keras.layers.Dense(1)
    prediction_layer(feature_batch_average)

    inputs = tf.keras.Input(shape=(img_width, img_height, 3))
    x = preprocess_input(inputs)
    x = base_model(x, training=False)
    x = global_average_layer(x)
    x = tf.keras.layers.Dropout(0.2)(x)
    outputs = prediction_layer(x)
    model = tf.keras.Model(inputs, outputs)

    base_learning_rate = 0.0001
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=base_learning_rate),
                  loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
                  metrics=["accuracy"])

    return model

# LOADING DATA

# Used to stop printing validation size from image_data_from_directory
sys.stdout = open(os.devnull, 'w') 


# Train + Validation
train_ds = tf.keras.utils.image_dataset_from_directory(
    data_dir,
    validation_split=TEST_SPLIT,
    subset="training",
    shuffle=True,
    seed=SEED,
    image_size=(img_height, img_width),
    batch_size=10000000,
    label_mode='binary'
)

# Augmented data
train_aug = tf.keras.utils.image_dataset_from_directory(
    aug_path,
    shuffle=True,
    seed=SEED,
    image_size=(img_height, img_width),
    batch_size=10000000,
    label_mode='binary'
)

# Test set
test_ds = tf.keras.utils.image_dataset_from_directory(
    data_dir,
    validation_split=TEST_SPLIT,
    subset="validation",
    seed=SEED,
    shuffle=True,
    image_size=(img_height, img_width),
    batch_size=10000000,
    label_mode='binary'
)

sys.stdout = sys.__stdout__


# Splitting data from labels

# Training data
x_train = None
y_train = None

for image, label in tfds.as_numpy(train_ds):
    x_train = image
    y_train = label

# Adding augmented data to the training set
for image, label in tfds.as_numpy(train_aug):
    x_train = np.append(x_train, image, axis=0)
    y_train = np.append(y_train, label, axis=0)


# Test data
x_test = None
y_test = None


for image, label in tfds.as_numpy(test_ds):
    x_test = image
    y_test = label

print("Validation  split: " + str(VALIDATION_SPLIT))
print("Test split: " + str(TEST_SPLIT))

print("\nFound " + (str(len(x_train) + len(x_test))) + " files")
print("Training size: " + str(len(x_train)))
print("Test size: " + str(len(x_test)) + "\n")

# Inverse of number of samples
def get_weights_inverse(no_of_classes, samples_per_cls, power=1):
    weights_for_samples = 1.0 / np.array(np. power(samples_per_cls, power))
    weights_for_samples = weights_for_samples / np.sum(weights_for_samples)*no_of_classes
    return weights_for_samples

# Inverse of Square Root of Number of Samples
def get_weights_inverse_square(no_of_classes, samples_per_cls, power=1):
    weights_for_samples = 1.0 / np.array(np. power(samples_per_cls, power))
    weights_for_samples = weights_for_samples / \
        np.sum(weights_for_samples)*no_of_classes
    return weights_for_samples

# Effective Number of Samples
def get_weights_effective_num_of_samples(no_of_classes, beta, samples_per_cls):
    effective_num = 1.0 - np.power(beta, samples_per_cls)
    weights_for_samples = (1.0- beta) / np.array(effective_num)
    weights_for_samples = weights_for_samples/np.sum(weights_for_samples)*no_of_classes
    return weights_for_samples

n_samples = len(y_train)
class_count = [0] * 2

for y in y_train:
    if y == 0:
        class_count[0] += 1
    else:
        class_count[1] += 1


inputs = x_train
targets = y_train


# SKlearn balanced class weights
# weights = class_weight.compute_class_weight("balanced", classes=np.unique(y_train), y=y_train[:, 0])
# weights = get_weights_inverse(2, class_count)
weights = get_weights_effective_num_of_samples(2, BETA, class_count)

class_weight = {0: weights[0], 1: weights[1]}
print("Weight per class: " + str(class_weight))

for train, test in kfold.split(inputs,targets):
    model = ModelBuilder()
    history = model.fit(inputs[train],targets[train], epochs=MAX_EPOCHS, validation_split=VALIDATION_SPLIT, class_weight=class_weight)
    scores = model.evaluate(inputs[test],targets[test],verbose=0)
    print(f'\nScore for fold {n_fold}: {model.metrics_names[0]} of {scores[0]}, {model.metrics_names[1]} of {scores[1]*100}%\n')
    acc_per_fold.append(scores[1]*100)
    loss_per_fold.append(scores[0])
    n_fold = n_fold+1

    y_pred = model.predict(inputs[test])
    y_pred = [1 if v >= 0 else 0 for v in y_pred]
    y_pred = np.round(y_pred)
    pred_per_fold.append(y_pred)

decimals = 3

# == Provide average scores ==
print("---------------------------------------------")
print('Score per fold')
for i in range(0, len(acc_per_fold)):
  print("---------------------------------------------")
  print(f'> Fold {i+1} - Loss: {round(loss_per_fold[i],decimals)} - Accuracy: {round(acc_per_fold[i],decimals)}%')
print("---------------------------------------------")
print('Average scores for all folds:')
print(f'> Accuracy: {round(np.mean(acc_per_fold),decimals)} (+- {round(np.std(acc_per_fold),decimals)})')
print(f'> Loss: {round(np.mean(loss_per_fold),decimals)}')
print("---------------------------------------------")


print("\n Evaluating the model with test set...")
eval_result = model.evaluate(x_test, y_test)
print("\nTest results:\n  - Test accuracy: " + str(round(eval_result[1], decimals)))
print("  - Test loss: " + str(round(eval_result[0], decimals)))
print("\n")


y_pred = model.predict(x_test)
y_pred = [1 if v >= 0 else 0 for v in y_pred]
y_pred = np.round(y_pred)

# Confusion matrix
print("\nConfusion matrix:")
cnf_m = confusion_matrix(y_true=y_test, y_pred=y_pred)
print(cnf_m)
plot_confusion_matrix(cnf_m, classes=[1, 0])
plt.show()

# Classification Report
print("\nClassification report:\n")
print(classification_report(y_true=y_test, y_pred=y_pred))



# model.save('saved_model/mobilnetv2')
